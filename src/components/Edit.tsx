import React from "react";
import { Button, Form, Input } from "antd";

interface ListType {
  list_id: string | number;
  title: string;
  created_at: Date;
  loading: boolean;
  edit_mode: boolean;
}

interface ChildProps {
  id: string | number;
  title: string;
  onCallback: (
    message: string,
    result: ListType | null,
    status: boolean,
    type: string
  ) => void;
}

const App: React.FC<ChildProps> = (props) => {
  const [form] = Form.useForm();

  const onFinish = async (values: { title: string }) => {
    try {
      // create list
      const response = await fetch(
        `https://dolphin-app-m9ox5.ondigitalocean.app/list/${props.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(values),
        }
      );

      if (response.ok) {
        const result: ListType = await response.json();
        result.edit_mode = false;
        result.loading = false;

        // Callback to the parent component
        props.onCallback(
          `Todo item #${props.id} updated successfully`,
          result,
          true,
          "edit"
        );
      } else {
        const result: ListType | null = null;
        props.onCallback(
          `Failed to update item #${props.id}`,
          result,
          false,
          "edit"
        );
      }
    } catch (error) {
      const result: ListType | null = null;
      props.onCallback("Failed to create item", result, false, "edit");
    }
  };

  return (
    <Form
      layout="inline"
      form={form}
      initialValues={{ title: props.title }}
      onFinish={onFinish}
    >
      <Form.Item label="Title" name="title" rules={[{ required: true }]}>
        <Input placeholder="Input placeholder" />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Update
        </Button>
      </Form.Item>
    </Form>
  );
};

export default App;
