import React from "react";
import { Button, Form, Input } from "antd";

interface ListType {
  list_id: string | number;
  title: string;
  created_at: Date;
  loading: boolean;
  edit_mode: boolean;
}

interface ChildProps {
  onCallback: (
    message: string,
    result: ListType | null,
    status: boolean,
    type: string
  ) => void;
}

const App: React.FC<ChildProps> = (props) => {
  const [form] = Form.useForm();

  const onFinish = async (values: { title: string }) => {
    try {
      // create list
      const response = await fetch(
        `https://dolphin-app-m9ox5.ondigitalocean.app/list`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(values),
        }
      );
      console.log(response, "error");
      if (response.ok) {
        const result: ListType = await response.json();

        // Callback to the parent component
        props.onCallback(
          "Todo item created successfully",
          result,
          true,
          "create"
        );
      } else {
        const result: ListType | null = null;
        props.onCallback("Failed to create item", result, false, "create");
      }
    } catch (error) {
      const result: ListType | null = null;
      props.onCallback("Failed to create item", result, false, "create");
    }
  };

  return (
    <Form
      layout="inline"
      form={form}
      initialValues={{ layout: "inline" }}
      onFinish={onFinish}
    >
      <Form.Item label="Title" name="title" rules={[{ required: true }]}>
        <Input placeholder="Input placeholder" />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Create
        </Button>
      </Form.Item>
    </Form>
  );
};

export default App;
