import React, { useState, useEffect, useMemo } from "react";
import "./App.css";
import { Divider, List, Skeleton, notification } from "antd";
import Add from "./components/Add";
import Edit from "./components/Edit";

interface ListType {
  list_id: string | number;
  title: string;
  created_at: Date;
  loading: boolean;
  edit_mode: boolean;
}

const Context = React.createContext({ name: "Default" });

function App() {
  const [initLoading, setInitLoading] = useState(true);
  const [data, setData] = useState<ListType[]>([]);
  const [api, contextHolder] = notification.useNotification();

  useEffect(() => {
    // get list
    fetch("https://dolphin-app-m9ox5.ondigitalocean.app/list")
      .then((res) => res.json())
      .then((res) => {
        if (res) {
          const transformedList = res.map((r: ListType) => ({
            list_id: r.list_id,
            title: r.title,
            created_at: r.created_at,
            edit_mode: false,
            loading: false,
          }));

          setData(transformedList);
        }
        setInitLoading(false);
      });
  }, []);

  // edit button
  const editBtn = (id: string | number) => {
    setData((prevData) =>
      prevData.map((item: ListType) =>
        item.list_id === id ? { ...item, edit_mode: true } : item
      )
    );
  };

  // update button
  const cancelBtn = (id: string | number) => {
    const result = data.filter((d) => d.list_id === id);
    const newData = (result[0].edit_mode = false);

    setData((prevData) =>
      prevData.map((item) =>
        item.list_id === id ? { ...item, newData } : item
      )
    );
  };

  // delete button
  const deleteBtn = async (id: string | number) => {
    try {
      // delete list by id
      const response = await fetch(
        `https://dolphin-app-m9ox5.ondigitalocean.app/list/${id}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (response.ok) {
        setData((prevData) => prevData.filter((item) => item.list_id !== id));
        // notification success
        api.success({
          message: `Todo item #${id} deleted successfully`,
          placement: "topRight",
        });
      } else {
        // notification error
        api.error({
          message: "Error",
          description: `Failed to delete item: ${response.status} ${response.statusText}`,
          placement: "topRight",
        });
      }
    } catch (error) {
      console.error("Error deleting:", error);
      // Handle errors
    }
  };

  const contextValue = useMemo(() => ({ name: "Ant Design" }), []);

  // handle create/edit callback
  const handleCallback = (
    message: string,
    result: ListType | null,
    status: boolean,
    type: string
  ) => {
    if (status) {
      if (result && type == "create") {
        // Update the state by adding the new todo item to the existing data
        setData((prevData) => [...prevData, result]);
      } else if (result && type == "edit") {
        setData((prevData) =>
          prevData.map((item) =>
            item.list_id === result.list_id ? { ...item, ...result } : item
          )
        );
      }

      api.success({
        message,
        placement: "topRight",
      });
    } else {
      api.error({
        message,
        placement: "topRight",
      });
    }
  };

  return (
    <>
      {/* Add list form */}
      <div className="card">
        <Divider orientation="left">Create todo list</Divider>
        <Add onCallback={handleCallback} />
      </div>
      {/* Add list form */}

      {/* Todo list */}
      <Context.Provider value={contextValue}>
        {contextHolder}
        <div className="card">
          <Divider orientation="left">Todo List</Divider>
          <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            bordered
            dataSource={data}
            loading={initLoading}
            renderItem={(item) => (
              <List.Item
                actions={[
                  item.edit_mode ? (
                    <a
                      key="list-loadmore-update"
                      onClick={() => cancelBtn(item.list_id)}
                    >
                      cancel
                    </a>
                  ) : (
                    <a
                      key="list-loadmore-edit"
                      onClick={() => editBtn(item.list_id)}
                    >
                      edit
                    </a>
                  ),
                  <a
                    key="list-loadmore-delete"
                    onClick={() => deleteBtn(item.list_id)}
                  >
                    delete
                  </a>,
                ]}
              >
                <Skeleton title={false} loading={item.loading} active>
                  {item.edit_mode ? (
                    <Edit
                      onCallback={handleCallback}
                      title={item.title}
                      id={item.list_id}
                    />
                  ) : (
                    <List.Item.Meta
                      title={<p>#{item.list_id}</p>}
                      description={item.title}
                    />
                  )}
                </Skeleton>
              </List.Item>
            )}
          />
        </div>
      </Context.Provider>
      {/* Todo list */}
    </>
  );
}

export default App;
